import { Router } from 'express'
import PaymentsService from '../services/payments.sevice.js'
// service payments

const products = [
    { id: 1, name: "papas", price: 1000 },
    { id: 2, name: "queso", price: 500 },
    { id: 3, name: "hamburguesa", price: 1500 },
    { id: 4, name: "soda", price: 1000 },
    { id: 5, name: "golosinas", price: 800 }
]

const router = Router()

router.post('/payment-intents', async (req, res)=>{
    const productRequested = products.find(product => product.id === Number(req.query.id))

    if(!productRequested) return res.status(404).send({
        status: 'error',
        error: 'Product not found'
    })
    
    const paymentIntentInfo = {
        amount: productRequested.price,
        currency: 'usd',
        metadata:{
            userId:'id.autogenerado-por-mongo',
            orderDetails: JSON.stringify({
                [productRequested.name]: 2
            },null, '\t'),
            address: JSON.stringify({
                street: 'Calle de prueba',
                postalCode: '3213',
                externalNumber: '123123'
            }, null, '\t')
        }
    }

    const service = new PaymentsService()
    let result = await service.createPaymentIntent(paymentIntentInfo)
    console.log(result)
    res.send({
        status: 'success', 
        payload: result
    })
})

export default router