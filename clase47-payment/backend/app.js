import express from 'express';
import mongoose from 'mongoose';
import cookieParser from 'cookie-parser';

import usersRouter from './routes/users.router.js';
import petsRouter from './routes/pets.router.js';
import adoptionsRouter from './routes/adoption.router.js';
import sessionsRouter from './routes/sessions.router.js';
import paymentsRouter from './routes/payments.router.js';
import dotenv from 'dotenv'
import cors from 'cors'
// swagger 
import swaggerJsDoc from 'swagger-jsdoc'
import swaggerUiExpress from 'swagger-ui-express'

dotenv.config()
const app = express();
const PORT = process.env.PORT||8080;
const connection = mongoose.connect('mongodb://localhost:27017/adoptame39730')

app.use(express.json());
app.use(cookieParser())
app.use(cors())

// const swaggerOptions = {
//     definition: {
//         openapi: '3.0.1',
//         info: {
//             title: 'Documentación de app Adoptame',
//             description: 'Api pensada para adopción de mascotas'
//         }
//     },
//     apis: [`${__dirname}/docs/**/*.yaml`]
// }

// const specs = swaggerJsDoc(swaggerOptions)

// endpoint de la documentación
// app.use('/docs', swaggerUiExpress.serve, swaggerUiExpress.setup(specs))

app.use('/api/payments', paymentsRouter)
app.use('/api/users',usersRouter);
app.use('/api/pets',petsRouter);
app.use('/api/adoptions',adoptionsRouter);
app.use('/api/sessions',sessionsRouter);

app.listen(PORT,()=>console.log(`Listening on ${PORT}`))
