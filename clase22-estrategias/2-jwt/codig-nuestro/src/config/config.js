const MongoStore = require('connect-mongo')
const { connect } = require('mongoose')

// const url = 'mongodb+srv://federicoaosandon:Federico1@cluster0.an130di.mongodb.net/coderhouse?retryWrites=true&w=majority'
const url = 'mongodb://localhost:27017/comision32270'

let configObject = {
    dbConnection:  async () => {
        try {
            await connect(url)
            console.log('DB conectada')  
        } catch (error) {
            console.log(error)
            process.exit()
        }        
    },
    session: {
        store: MongoStore.create({
            mongoUrl: 'mongodb://localhost:27017/comision32270',
            mongoOptions: {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            },
            ttl: 15000000000
        }), 
        secret: 's3cr3t0',
        resave: false,
        saveUninitialized: false,
    }
}

module.exports = { configObject }