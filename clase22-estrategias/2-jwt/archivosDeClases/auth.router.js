const { Router } = require('express')
const { UserModel } = require('../models/user.model')
const { generateToken } = require('../utils/jsonwt')

const router = Router()



//_________________________________________________________________________


router.post('/login', async (req, res)=>{
    const {email, password} = req.body
     console.log(email, password)
    // encripar la contraseña que viene del formulario, comparar con la encriptada de la base de datos
    // const user = await UserModel.findOne({email, password})
    const user = await users.find(user => user.email === email && user.password === password)


    if (!user) return res.status(401).send({status: 'error', error: 'Invalid credentials'})
    
    // req.session.user = {
    //     name: `${user.first_name} ${user.last_name}`,
    //     email: user.email
    // }

    const access_token = generateToken(user)

    res.status(200).send({
        status: 'success',
        access_token,
        message: 'Login correcto',
    })
})


router.post('/register', async (req, res)=>{ // con basae de datos
    const { first_name, last_name, email, password } = req.body

    // pregintar si existe el usuario
    // const exists = await UserModel.findOne({email})
    const exists = await users.find(user => user.email === email)

    if (exists) return res.status(401).send({status: 'error', error: 'El usuario ya existe'})

    const user = {
        first_name,
        last_name,
        email,
        password// lo vamos a ver la clase que viene
    }
    // let result = await UserModel.create(user)
    users.push(user)
    const access_token = generateToken(user)

    res.status(200).json({
        status: 'success',
        access_token
    })
})

module.exports = router
// export default router

// npm i express express-handlebars socket.io