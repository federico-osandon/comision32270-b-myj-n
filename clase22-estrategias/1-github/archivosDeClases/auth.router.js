const { Router } = require('express')
const passport = require('passport')
const { UserModel } = require('../models/user.model')

const router = Router()



//_________________________________________________________________________
router.get('/login', async (req, res)=>{    
    res.status(200).render('login')
})

router.get('/github', passport.authenticate('github',{scope: ['user:email']}))

router.get('/githubcallback', passport.authenticate('github', {failureRedirect: '/api/auth/login'}), async (req, res)=>{
    req.session.user = req.user
    res.redirect('/api/products')
})





module.exports = router
// export default router

// npm i express express-handlebars socket.io