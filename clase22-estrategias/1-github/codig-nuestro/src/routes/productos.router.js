const { Router } = require('express')
const { ProductModel } = require('../models/product.model')
// import { Router } from 'express'



const router = Router()


router.use((req, res, next)=>{
    console.log('Time: ', Date())
    next()
})

// GET api/productos /
router.get('/', async (request, response) =>{
    const productos = await ProductModel.find({})
    response.status(200).json({
        status: 'success',
        payload: productos
    })
})

// GET api/productos /
router.post('/', (request, response) =>{
    const {name, price} = request.body
    response.status(200).send({name, price})
})

module.exports = router
// export default router

