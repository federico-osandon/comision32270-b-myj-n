const chai = require('chai')
const mongoose= require('mongoose')
const User = require('../src/dao/Users.dao.js')

mongoose.connect('mongodb://localhost:27017/adoptame')
const expect = chai.expect

describe('Set de testy con Chai', ()=>{
    before(function () {
        this.userDao = new User()
    })
    beforeEach(function(){
        mongoose.connection.collections.users.drop()
        this.timeout(5000)
    })
    it('El dao debe poder obtener los usuarios en formato de arreglo', async function(){
        const result = await this.userDao.get({})
        console.log(result)
        // expect(result).to.be.deep.equal([])
        // expect(result).deep.equal([])
        // expect(Array.isArray(result)).to.be.ok
        expect(Array.isArray(result)).to.be.equal(true)
    })
})