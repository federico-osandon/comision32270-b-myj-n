const MongoStore = require('connect-mongo')
const { connect, set } = require('mongoose')

// const url = 'mongodb+srv://federicoaosandon:Federico1@cluster0.an130di.mongodb.net/coderhouse?retryWrites=true&w=majority'
const url = 'mongodb://localhost:27017/comision32270'

let configObject = {
    dbConnection:  async () => {
        try {
            set('strictQuery', false);
            connect(url,{
                useNewUrlParser: true, // conectividad y estabilidad
                useUnifiedTopology: true
            })
            console.log('DB conectada')  
        } catch (error) {
            console.log(error)
            process.exit()
        }        
    },
    session: {
        store: MongoStore.create({
            mongoUrl: 'mongodb://localhost:27017/comision32270',
            mongoOptions: {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            },
            ttl: 15000000000
        }), 
        secret: 's3cr3t0',
        resave: false,
        saveUninitialized: false,
    }
}

module.exports = { configObject }



// useNewUrlParser: true: Esta opción se utiliza para indicar a 
// Mongoose que debe usar el nuevo analizador de URL de MongoDB. 
// Anteriormente, MongoDB utilizaba un analizador de URL llamado 
// mongodb:// que está ahora en desuso. El nuevo analizador de URL es mongodb+srv:// 
// y se utiliza para conexiones de clúster. Al usar useNewUrlParser: true, 
// Mongoose puede procesar correctamente la URL de conexión a MongoDB incluso si utiliza 
// el nuevo analizador de URL.

// useUnifiedTopology: true: Esta opción se utiliza para habilitar la nueva implementación de 
// detección y monitoreo de estado de MongoDB. Anteriormente, Mongoose utilizaba una implementación 
// llamada Server Discovery and Monitoring engine (SDAM) que a veces no funcionaba 
// correctamente en algunas configuraciones de red. La nueva implementación se llama Unified 
// Topology y aborda muchos de los problemas que se encontraron con la implementación anterior. 
// Al usar useUnifiedTopology: true, Mongoose puede garantizar una conexión estable y confiable con 
// MongoDB.