// session -> login - register - logout
import {Router} from 'express'
import { UsersManagerMongo } from '../dao/usersManagerMongo.js'
import { auth } from '../middlewares/auth.middleware.js'

export const sessionsRouter = Router()

const userService = new UsersManagerMongo()

sessionsRouter.post('/register', async (req, res) => {
    try {
        const {first_name, last_name, email, password } = req.body
        
        // validar si vienen los datos
        if(!email || !password) return res.status(401).send({status: 'error', error: 'se debe completar todos los datos'})
    
        //validar si existe el usuario
        const userExist = await userService.getUserBy({email})
        if(userExist) return res.status(401).send({status: 'error', error: 'el usuario ya existe'})
    
        const newUser = {
            first_name,
            last_name, 
            email, 
            password // lo vamos a encriptar
        }
    
        const result = await userService.createUser(newUser)
        // validar el error
        console.log(result)
    
        res.send('usuario registrado')
        
    } catch (error) {
        console.log(error)
    }
})


sessionsRouter.post('/login', async (req, res) => {
    const {email, password} = req.body

    // validar si vienen los datos
    if(!email || !password) return res.status(401).send({status: 'error', error: 'se debe completar todos los datos'})

    const userFound = await userService.getUserBy({email, password})


    if(!userFound) return res.status(401).send({status: 'error', error: 'usuario no encontrado'})

    req.session.user = {
        email,
        admin: userFound.role === 'admin'
    }

    console.log(req.session.user)
    res.send('login success')
})

sessionsRouter.get('/current', auth, (req, res) => {
    res.send('datos sensibles')
})


sessionsRouter.get('/logout', (req, res) => {
    req.session.destroy( err => {
        if(err) return res.send({status: 'error', error: err})
        else return res.redirect('/login')
    })
})



