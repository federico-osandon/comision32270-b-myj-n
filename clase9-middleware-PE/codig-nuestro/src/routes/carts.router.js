const { Router } = require('express')

const router = Router()


// POST localhost/api/carts
router.post('/', async (req, res) => {
    const cart = req.body
    cart.id = Date.now()
    cart.product = []
    // metodo que tiene la clase para agregar el carrito al archivo
    res.status(200).json({
        cart,
        msg: 'Carrito creado'
    
    })
})

// GET localhost/api/carts/:cid
router.get('/:cid', async (req, res) => {
    const { cid } = req.params
    // con el id buscar el carrito
    cart={
        id: cid,
        product: []
    }
    res.status(200).json({
        msg: 'Tomá tu carrito',
        cart
    })
})

// POST localhost/api/carts/:cid/product/:pid

router.post('/:cid/product/:pid', async (req, res) => {
    const { cid, pid } = req.params
    const cart = {
        id: cid,
        product: [{
            id: pid, 
            quantity: 5
        }]
    }
    res.status(200).json({
        msg: 'Producto agregado al carrito',
        cart
    })
})


module.exports = router