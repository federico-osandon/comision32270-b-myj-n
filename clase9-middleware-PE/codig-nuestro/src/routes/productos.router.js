const { Router } = require('express')
const ProductManagerFile = require('../Daos/productManagerFile')
// import { Router } from 'express'



const router = Router()
const Products = new ProductManagerFile()

router.use((req, res, next)=>{
    console.log('Time: ', Date())
    next()
})

// GET api/productos /
router.get('/', async (request, response) =>{
    const products = await Products.getProducts()
    console.log(products)
    response.status(200).send(products)
})

// GET api/productos /
router.post('/', async (request, response) =>{
    const product = request.body
    const resp = await Products.addProduct(product)

    response.status(200).send({
        msg: resp,
        product
    })
})

module.exports = router
// export default router

