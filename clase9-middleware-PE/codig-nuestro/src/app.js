// const express = require('express')
const express = require('express')
const cookieParser = require('cookie-parser')
const usersRouter = require('./routes/users.router.js')
const productsRouter = require('./routes/productos.router.js')
const cartsRouter = require('./routes/carts.router.js')
const { uploader } = require('./utils.js')

const app = express()
const PORT = 8080

app.use(express.json())
app.use(express.urlencoded({extended:true}))
console.log(__dirname)
app.use('/virtual' ,express.static(__dirname+'/public'))
app.use(cookieParser())

app.use( (req, res, next)=>{
    console.log('Time: ', Date())
    next()
} )

function mid1(req, res, next) {
    req.dato1='dato uno '
    next()
}

function mid2(req, res, next) {
    req.dato2='dato dos '
    next()
}



// http://localhost:8080/api/usuarios
app.use('/api/usuarios', mid1, usersRouter)

// http://localhost:8080/api/products
app.use('/api/products', 
    // mid2, 
    // mid1, 
    productsRouter
)

// http://localhost:8080/api/productos
app.use('/api/carts', cartsRouter)

app.post('/single', uploader.single('myfile') ,(req, res)=>{
    res.status(200).json({
        mensaje: 'se a subido con éxito el archivo'
    })
})

app.use((err, req, res, next)=>{
    console.log(err)
    res.status(500).send('Todo mal')
})

app.listen(PORT,err =>{
    if (err)  console.log(err)
    console.log(`Escuchando en el puerto ${PORT}`)
})