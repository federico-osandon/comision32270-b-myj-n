const { connect } = require('mongoose')
const { CartModel } = require('../models/cart.model');
const { orderModel } = require('../models/order.model');
const { ProductModel } = require('../models/product.model')
const { UserModel } = require('../models/user.model')


const ordenes = [
    {
      name: "Margherita",
      size: "small",
      price: 8,
      quantity: 2,
      date: "2021-01-13T09:08:13Z"
    },
    {
      name: "Pepperoni",
      size: "medium",
      price: 12,
      quantity: 1,
      date: "2020-05-13T09:08:13Z"
    },
    {
      name: "Hawaiian",
      size: "medium",
      price: 16,
      quantity: 3,
      date: "2022-03-11T09:08:13Z"
    },

    {
        name: "Hawaiian",
        size: "large",
        price: 16,
        quantity: 3,
        date: "2022-03-14T09:08:13Z"
    },
    {
        name: "Margherita",
        size: "large",
        price: 16,
        quantity: 3,
        date: "2022-03-11T09:08:12Z"
    },
    {
        name: "Pepperoni",
        size: "large",
        price: 16,
        quantity: 3,
        date: "2022-03-15T09:08:13Z"
    },
    {
        name: "Pepperoni",
        size: "large",
        price: 25,
        quantity: 3,
        date: "2022-03-18T09:08:12Z"
    },
    {
        name: "Margherita",
        size: "large",
        price: 30,
        quantity: 3,
        date: "2022-03-21T09:08:12Z"
    }
  ];

// const url = 'mongodb+srv://federicoaosandon:Federico1@cluster0.an130di.mongodb.net/coderhouse?retryWrites=true&w=majority'
const url = 'mongodb://localhost:27017/comision32270'

const dbConnection = async () => {
    try {
        await connect(url)
        console.log('DB conectada')
        
        // insertar las ordenes
        // let result = await orderModel.insertMany(ordenes)
        // console.log(result)
        
        // solicitar todas las ordenes
        // let result = await orderModel.find({})
        // console.log(result)

        // stages -> pasos 
        // // localhost:8080/reporte-pizza?tamno=medium
    //     let tamannoPizza = 'medium'
    //     let orders = await orderModel.aggregate([
    //         {
    //             $match: { size: tamannoPizza } // filtra por tamaño
    //         },
    //         {
    //             $group: {_id: "$name", totalQuantity: {$sum: "$quantity"}} // arma un obj con las condiciones que le pedimos
    //         },
    //         {
    //             $sort: {totalQuantity: -1}
    //         },
    //         {
    //             $group: { _id: 1, orders: { $push: "$$ROOT" }}
    //         },
    //         {
    //             $project:{
    //                 "_id": 0,
    //                 orders: "$orders"
    //             }
    //         },
    //         {
    //             $merge: {
    //                 into: 'reports'
    //             }
    //         }

    // ])
        
    // console.log(orders)


    // consulta con paginate-v2
    // let users = await UserModel.paginate({gender: 'Female'},{limit:20,page:1})
    // console.log(users)

    } catch (error) {
        console.log(error)
        process.exit()
    }
    
}

module.exports = { dbConnection }