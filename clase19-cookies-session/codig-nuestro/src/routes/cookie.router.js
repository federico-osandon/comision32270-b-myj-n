const { Router } = require('express')

const router = Router()

router.get('/set', (req, res)=>{
    res.cookie('Coderhouse', 'Esta es una cookie', {maxAge: 10000000}).send('cookie seteada')
})

router.get('/setSigned', (req, res)=>{
    res.cookie('Coderhouse', 'Esta es una cookie', {maxAge:100000, signed: true}).send('cookie firmada')
})

// router.get('/get', (req, res)=>{
//     res.send(req.cookies)
// })
router.get('/get', (req, res)=>{
    res.send(req.signedCookies)
})

router.get('/delete', (req, res)=>{
    res.clearCookie('nombre').send('cookie borrada')
})

module.exports = router