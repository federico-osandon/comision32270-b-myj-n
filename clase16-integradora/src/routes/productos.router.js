const express = require('express')
const ProductModel = require('../models/productos.model.js')

const router = express.Router()


router.get('/', async (req, res) =>{
    // esto va a estar en unf método de productManager
    const products = await ProductModel.find({})

    res.json(products)
})

router.post('/', async (req, res) =>{
    const { title, price } = req.body
    // esto va a estar en unf método de productManager
    const products = await ProductModel.create({
        title,
        price
    })

    res.json(products)
})

module.exports = router