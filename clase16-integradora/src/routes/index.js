const { Router } = require('express')
const { uploader } = require('../utils/uploader.js')
const productsRouter = require('./productos.router.js')

const router = Router()

router.get('/', uploader.single('myflie'), (req, res)=>{
    res.send('Ruta raíz')
})

// Rutas para productos

router.use('/api/products', productsRouter)
// router.use('/api/carts', cartsRouter)
// router.use('/api/users', productsRouter)

module.exports = router // exportación por defecto