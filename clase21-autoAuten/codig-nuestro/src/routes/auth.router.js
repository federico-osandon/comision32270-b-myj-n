const { Router } = require('express')
const passport = require('passport')
const { UserModel } = require('../models/user.model')
const { createHash, isValidPassword } = require('../utils/bcryptPass')

const router = Router()

const products = [
    {title: 'Gorra rosa',  price: 400, imageUrl: 'https://cdn.palbincdn.com/users/31244/images/GORRA-BASICA-JUNIOR-CUSTOMIZASHOPBF10B-COLOR-ROSA-1611838353.jpg', category:'gorras'},
    {title: 'Gorra rosa',  price: 350, imageUrl: 'https://cdn.palbincdn.com/users/31244/images/GORRA-BASICA-JUNIOR-CUSTOMIZASHOPBF10B-COLOR-ROSA-1611838353.jpg', category:'gorras'},
    {title: 'Gorra rosa',  price: 300, imageUrl: 'https://cdn.palbincdn.com/users/31244/images/GORRA-BASICA-JUNIOR-CUSTOMIZASHOPBF10B-COLOR-ROSA-1611838353.jpg', category:'gorras'},
    {title: 'Gorra rosa',  price: 200, imageUrl: 'https://cdn.palbincdn.com/users/31244/images/GORRA-BASICA-JUNIOR-CUSTOMIZASHOPBF10B-COLOR-ROSA-1611838353.jpg', category:'gorras'},
    {title: 'Gorra rosa',  price: 150, imageUrl: 'https://cdn.palbincdn.com/users/31244/images/GORRA-BASICA-JUNIOR-CUSTOMIZASHOPBF10B-COLOR-ROSA-1611838353.jpg', category:'gorras'}
]

router.get('/', async (req, res)=>{    
    let testUser = {
        name: 'Federico',
        last_name: 'Osandón',
        role: 'admin',
    }
    

    // req.session.user = testUser.name
    // req.session.admin = true

    res.status(200).render('index', {
        user: testUser,
        isAdmin: testUser.role==='admin',
        products,
        style: 'index.css'
    })
})


router.get('/login', async (req, res)=>{
    // res.status(200).render('login')
    res.status(200).render('login')
})

router.post('/login', passport.authenticate('login', {failureRedirect: '/faillogin'}),async (req, res)=>{
    if(!req.user) return res.status(400).json({status: 'error', error: 'Credentials incorrect'})
    
    req.session.user = {
        name: req.user.first_name,
        last_name: req.user.last_name,
        email: req.user.email,
    }


    res.status(200).send({
        status: 'success',
        payload: req.user,
        message: 'Login correcto',
    })
})

router.get('/faillogin', async (req, res)=>{
    res.status(400).json({error: 'failed login'})
})

router.get('/register', async (req, res)=>{
    
    res.status(200).render('register')
})

router.post('/register',  passport.authenticate('register', {failureRedirect: '/failregister'}), async (req, res)=>{ // con basae de datos
   
    res.status(200).json({
        status: 'success',
        message: 'Usuario registrado correctamente'
    })
})

router.get('/failregister', async (req, res)=>{
    console.log('failregister')
    res.status(400).json({error: 'failed register'})
})


router.post('/restaurarpass', async (req, res) => {
    const { email, password } = req.body;
  
    // Encontrar el usuario por correo electrónico
    const user = await UserModel.findOne({ email });
  
    if (!user) {
      // Si el usuario no existe, redireccionar a una página de error
      return res.status(401).send({status: 'error', message: 'El usuario no existe'})
    }    
  
    //Hasear Actualizar la contraseña del usuario
    user.password = createHash(password)
    await user.save()
  
    // Redireccionar al usuario a la página de login
    res.status(200).json({status: 'success', message:'Contraseña actualizada correctamente'});
  })

router.get('/logout', async (req, res)=>{
    // session.destroy()
    req.session.destroy(err => {
        if(err) return res.send({status:'Logout error', message: err})           
    })
    res.status(200).redirect('/login')
})

module.exports = router
// export default router

// npm i express express-handlebars socket.io