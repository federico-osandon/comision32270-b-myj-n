const { Router } = require('express')
const { UserModel } = require('../models/user.model')
const { createHash, isValidPassword } = require('../utils/bcryptPass')

const router = Router()

const products = [
    {title: 'Gorra rosa',  price: 400, imageUrl: 'https://cdn.palbincdn.com/users/31244/images/GORRA-BASICA-JUNIOR-CUSTOMIZASHOPBF10B-COLOR-ROSA-1611838353.jpg', category:'gorras'},
    {title: 'Gorra rosa',  price: 350, imageUrl: 'https://cdn.palbincdn.com/users/31244/images/GORRA-BASICA-JUNIOR-CUSTOMIZASHOPBF10B-COLOR-ROSA-1611838353.jpg', category:'gorras'},
    {title: 'Gorra rosa',  price: 300, imageUrl: 'https://cdn.palbincdn.com/users/31244/images/GORRA-BASICA-JUNIOR-CUSTOMIZASHOPBF10B-COLOR-ROSA-1611838353.jpg', category:'gorras'},
    {title: 'Gorra rosa',  price: 200, imageUrl: 'https://cdn.palbincdn.com/users/31244/images/GORRA-BASICA-JUNIOR-CUSTOMIZASHOPBF10B-COLOR-ROSA-1611838353.jpg', category:'gorras'},
    {title: 'Gorra rosa',  price: 150, imageUrl: 'https://cdn.palbincdn.com/users/31244/images/GORRA-BASICA-JUNIOR-CUSTOMIZASHOPBF10B-COLOR-ROSA-1611838353.jpg', category:'gorras'}
]

router.get('/', async (req, res)=>{    
    let testUser = {
        name: 'Federico',
        last_name: 'Osandón',
        role: 'admin',
    }
    

    // req.session.user = testUser.name
    // req.session.admin = true

    res.status(200).render('index', {
        user: testUser,
        isAdmin: testUser.role==='admin',
        products,
        style: 'index.css'
    })
})


router.get('/login', async (req, res)=>{
    // res.status(200).render('login')
    res.status(200).render('login')
})

router.post('/login', async (req, res)=>{

    const {email, password} = req.body
     console.log(email, password)

     if (!email || !password) return res.status(401).send({status: 'error', message: 'todos los campos son obligatorios'})

    // encripar la contraseña que viene del formulario, comparar con la encriptada de la base de datos
    const user = await UserModel.findOne({email})
    //me trajo el usuario
    // console.log(isValidPassword(user, password))
    
    if (!isValidPassword(user, password)) {        
        return res.status(401).send({status: 'error', message: 'Usuario o contraseña incorrectos'})    
        
    }

    
    req.session.user = {
        name: `${user.first_name} ${user.last_name}`,
        email: user.email
    }

    res.status(200).send({
        status: 'success',
        payload: req.session.user,
        message: 'Login correcto',
    })
})

router.get('/register', async (req, res)=>{
    
    res.status(200).render('register')
})

router.post('/register', async (req, res)=>{ // con basae de datos
    const { first_name, last_name, email, password } = req.body

    
    if (!first_name || !last_name || !email || !password ) {
        return res.status(401).send({status: 'error', message: 'Todos los campos son obligatorios'})
    }
    
    // pregintar si existe el usuario
    const exists = await UserModel.findOne({email})
    if (exists) return res.status(401).send({status: 'error', message: 'El usuario ya existe'})

    // otra forma de crear el usuario
    const user = {
        first_name,
        last_name,
        email,
        password: createHash(password) // lo vamos a ver la clase que viene
    }
    let result = await UserModel.create(user)

    res.status(200).json({
        status: 'success',
        payload: result
    })
})

router.post('/restaurarpass', async (req, res) => {
    const { email, password } = req.body;
  
    // Encontrar el usuario por correo electrónico
    const user = await UserModel.findOne({ email });
  
    if (!user) {
      // Si el usuario no existe, redireccionar a una página de error
      return res.status(401).send({status: 'error', message: 'El usuario no existe'})
    }    
  
    //Hasear Actualizar la contraseña del usuario
    user.password = createHash(password)
    await user.save()
  
    // Redireccionar al usuario a la página de login
    res.status(200).json({status: 'success', message:'Contraseña actualizada correctamente'});
  })

router.get('/logout', async (req, res)=>{
    // session.destroy()
    req.session.destroy(err => {
        if(err) return res.send({status:'Logout error', message: err})           
    })
    res.status(200).redirect('/login')
})

module.exports = router
// export default router

// npm i express express-handlebars socket.io