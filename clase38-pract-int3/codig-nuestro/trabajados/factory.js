const {persistence} = require("../config/config");

let ProductDao
let UserDao
let CartDao
let OrderDao

switch (persistence) {
    case 'mongo':
        // 'ProductManager'
        const PoductDaoMongo  = require('./Mongo/prodcut.mongo')
        ProductDao = PoductDaoMongo

        const UserDaoMongo = require('./Mongo/user.mongo')
        UserDao = UserDaoMongo
        break;
    case 'archivo':
        
        break;
    case 'memory':
        
        break;

    default:
        break;
}

module.exports = {
    ProductDao,
    UserDao
}