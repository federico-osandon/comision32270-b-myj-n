const MongoStore = require('connect-mongo')
const mongoose = require('mongoose')
const dotenv =  require('dotenv')
const { commander } = require('../utils/commander')

// let url = 'mongodb://localhost:27017/comision32270'
const {mode} = commander.opts()
const enviroment = mode || 'development'
console.log(mode)

dotenv.config({
    path: mode === 'development' ? './.env.development' : './.env.production'
    // path: './env.production'
})
let mongo_url=process.env.MONGO_URL
// console.log(mongo_url)

module.exports = {
    port: process.env.PORT || '',
    jwt_secret_key: process.env.JWT_SECRET || '',
    mail_password: process.env.MAil_PASSWORD || '', 
    mail_admin: 'defe014@gmail.com',
    persistence: process.env.PERSISTENCE,
    dbConnection: async ()=>{
        try {
            // set('stictQuery', set) // sacar leyenda en la consola de deprecado
            await mongoose.connect(mongo_url, {
                useNewUrlParser: true,   // mongodb mongodb+srv://
                useUnifiedTopology: true
            })
            // conección base de dato
            console.log('base de dato conectada')
            
        } catch (error) {
            console.log(error)
        }
    },
    session: {
        store: MongoStore.create({  // new MongoStore === new require('connect-mongo')
            mongoUrl: mongo_url,
            mongoOptions: {
                useNewUrlParser: true,   // mongodb mongodb+srv://
                useUnifiedTopology: true
            },
            ttl: 10000000000000
        }),
        secret: 'Secreto',
        resave: false,
        // saveUnitialized: false
    }
}
