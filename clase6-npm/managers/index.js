const ManagerUsuarios = require("./ManagerUsuarios")


const manager = new ManagerUsuarios();


const env = async() =>{
    let primeraConsultaUsuarios = await manager.consultarUsuarios();
    console.log(primeraConsultaUsuarios); //Debe devolver vacío

    let user = {
        nombre:"Federico 1",
        apellido:"OSandon",
        edad:26,
        curso:"Backend",
        contrasena: '123'
    }

    // let result = await manager.crearUsuario(user);
    // console.log(result); //Debe devolver al usuario con un id


    let segundaConsultaUsuarios = await manager.consultarUsuarios();
    console.log(segundaConsultaUsuarios); //Debe devolver al usuario instertado  
    
    manager.validarUsuario('Federico 2','123')
}
env();