// const express = require('express')
const express = require('express')
const cookieParser = require('cookie-parser')
const handlebars = require('express-handlebars')
const logger = require('morgan')
// session_______________________________________________________________
const session = require('express-session')
const cors = require('cors')
// socket io _______________________________________________________________
const { configObject } = require('./config/config.js')
// socket io _______________________________________________________________
// require('dotenv').config()

const { Server: HttpServer } = require('http')
const { Server: ServerIo } = require('socket.io')
const { initProductsSocket } = require('./utils/productsSocketIo.js')
const { router } = require('./routes')
// _____________________________________________________________________
const { initializePassport } = require('./config/passport.config.js')
const passport = require('passport')
const compression = require('express-compression')



const app = express()
const httpServer = new HttpServer(app)
const io = new ServerIo(httpServer)

// oncección con la base de datos mongo __________________________________________________________________
configObject.dbConnection()

app.use('/virtual' ,express.static(__dirname+'/public'))
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors())
app.use(cookieParser())

// passport _______________________
initializePassport()
app.use(passport.initialize())
// _________________________________________________________

app.use(logger('dev'))

// session mongo_______________________________________________________________
app.use(session(configObject.session))


// handlebars_______________________________________________________________
app.engine('handlebars', handlebars.engine())
app.set('views', __dirname+'/views')
app.set('view engine', 'handlebars')
 

app.use(router)

// app.use(compression())
app.use(compression({
    brotli: {
        enabled: true, 
        zlib: {}
    }
}))
app.get('/stringmuylargo', (req,res)=>{
    let string = 'Hola Coders, soy un string ridículamente largo.'
    for(let i= 0; i<5e4; i++){
        string += `Hola Coders, soy un string ridículamente largo.`
    }
    res.send(string)
})

app.use((err, req, res, next)=>{
    console.log(err)
    res.status(500).send('Todo mal')
})

// socket_______________________________________________________________
initProductsSocket(io)

module.exports = {
    httpServer
}


