const EErrors = require("../../Errors/enum");

const errorHandler = (error, req, res, next)=>{
    console.log('midd: ', error)
    switch (error.code) {
        case EErrors.INVALID_TYPES_ERROR:
                res.send({status: 'error', error: error.name})
            break;
    
        default:
            res.send({status: 'error', error: 'Unhandled error'})
            break;
    }
    next()
}

module.exports = errorHandler