class CustomError {
    static createError({name= 'Error', cause, message, code}){
        const error = new Error(message)// (error, text='prueba.js', 1)
        error.cause = cause
        error.name = name
        error.code = code
        throw error.cause
    }
}

module.exports = CustomError

// try {
    // CustomError.createError()
// } catch (error) {
//     console.log(error)
// }