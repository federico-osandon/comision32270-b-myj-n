
const { configObject } = require("./config/config.js")
const { httpServer } = require("./server.js")
// NODE_ENV = undefine
// cli -> set NODE_ENV=production
// .env -> NODE_ENV=production
// app.js -> NODE_ENV='production'

const PORT = configObject.PORT

httpServer.listen(PORT,err =>{
    if (err)  console.log(err)
    console.log(`Escuchando en el puerto ${httpServer.address().port }`)
})