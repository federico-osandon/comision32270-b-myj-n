const {Schema, model} = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

let collection = 'usuarios'

const userSchema = new Schema({
    first_name: {
        type: String,        
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        index: true,
        required: true,
        unique: true
    },
    password: String,
    role: {
        type: String,
        enum: ['admin', 'user'],
        default: 'user'
    }
})

userSchema.plugin(mongoosePaginate)

let UserModel= model(collection, userSchema)

module.exports = {
    UserModel
}