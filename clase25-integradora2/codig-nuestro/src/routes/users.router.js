const { Router } = require('express')
const { authorization } = require('../middleware/authorizationPassport')

const { passportAuth } = require('../middleware/passportAuth')
const { UserModel } = require('../models/userSchema')
// import { Router } from 'express'

const router = Router()

// get http://localhost:8080/api/usuarios /
router.get('/', passportAuth('jwt'), authorization('admin') ,async (req, res) =>{
    const {page=1} = req.query
    const { docs } = await UserModel.paginate({}, {limit: 2, page, lean: true}) // ojo acá lean convierte a obj js

    res.status(200).send(docs)
})

// get http://localhost:8080/api/usuarios /id
router.get('/:id', (request, response) =>{
    const {id} = request.params
    response.status(200).send(id)
})



// POST http://localhost:8080/api/usuarios /
router.post('/', (request, response) =>{
    //mada el  cliente request 
    let user = request.body
    if (!user.nombre || !user.apellido) {
        return response.status(400).send({ message: 'Che pasar todos los datos'})
    }
    
    response.status(201).send({ 
        user,
        message: 'usuario creado' 
    })
})

// PUT http://localhost:8080/api/usuarios /:userId
// router.put('/:userId', (request, response) =>{

//     const { userId } = request.params
//     // venga el id
//     const index = arrayUsuarios.findIndex(user => user.id === userId)
//     // exista el usuario 
//     if (index === -1) {
//         return response.status(400).send({ message: 'No se encuentra el usuario'})
//     }

//     //mada el  cliente request 
//     let user = request.body
//     if (!user.nombre || !user.apellido) {
//         return response.status(400).send({ message: 'Che pasar todos los datos'})
//     }

//     // console.log('user post',user)
//     arrayUsuarios[index] = user
//     console.log(arrayUsuarios)

//     response.status(201).send({ 
//         users: arrayUsuarios,
//         message: 'usuario Modificado' 
//     })
// })

// DELETE http://localhost:8080/api/usuarios /:userId
// router.delete('/:userId', (req, res)=> {
//     const { userId } = req.params

//     let arrayTamanno = arrayUsuarios.length
//     console.log(arrayTamanno)
//     let users = arrayUsuarios.filter(user => user.id !== userId )
//     console.log(users.length)
//     if (users.length === arrayTamanno) {
//         res.status(404).send({ message:"Usuario no encontrado" })
//     }
//     res.status(200).send({ message:"Usuario borrado", users })
// })

module.exports = router
// export default router



