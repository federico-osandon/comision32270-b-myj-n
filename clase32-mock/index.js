const { Router } = require('express')
const fakerRouter = require('./faker.router.js')

const router = Router()


router.use('/faker', fakerRouter)



module.exports = router