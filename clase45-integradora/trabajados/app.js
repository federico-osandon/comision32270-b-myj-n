const express = require('express' )
const mongoose = require('mongoose' )
const cookieParser = require('cookie-parser' )


///Logger 
const { logger } = require('./config/logger.config.js')
const { addLogger } = require('./middlewars/logger.middleware.js')

// Swagger
const swaggerJsDoc = require('swagger-jsdoc')
const swaggerUiExpress = require('swagger-ui-express')
const { swaggerOptions } = require('./config/swagger.config.js')
const cors = require('cors')

const app = express() 
const PORT = process.env.PORT||8080 
const connection = mongoose.connect(`mongodb://localhost:27017/adoptame`)

app.use(express.json())
app.use(cookieParser())
app.use(addLogger)
app.use(cors())

const specs = swaggerJsDoc(swaggerOptions)
app.use('/docs', swaggerUiExpress.serve, swaggerUiExpress.setup(specs))



app.listen(PORT,()=> logger.info(`Listening on ${PORT}`) )

