const chai      = require('chai')
const supertest = require('supertest')

const expect    = chai.expect
const requester = supertest('http://localhost:8080')

describe('Test CRUD para Pets', () => {
    let petId

    it('GET para Pets', async () => {
        const response = await requester.get('/api/pets')

        expect(response.status).to.equal(200)
        expect(response.body).to.have.property('payload')
        expect(response.body.payload).to.be.an('array')
    })
    it('GET con Id para una mascota Pet', async () => {
        petId = '64791d90e2c0f556ceef6f5f'
        const response = await requester.get(`/api/pets/${petId}`)

        expect(response.status).to.equal(200)
        expect(response.body).to.have.property('payload')
        expect(response.body.payload).to.have.property('_id', petId)
    })
    it('PUT para Pets', async () => {

    })
    it('DELETE para Pets', async () => {

    })

})