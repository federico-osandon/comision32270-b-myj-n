// const fs = require('fs')

// console.log(fs)

/// sincrónico ______________________________________________________________________________



// fs.writeFileSync('./data.txt', 'esto es un ejemplo', 'utf-8' )

// console.log(fs.existsSync('./data.txt'));

// fs.appendFileSync('./data.txt', ' \n esto es un agregado', 'utf-8')

// const archivo = fs.readFileSync('./data.txt', 'utf-8')
// console.log(archivo);
// fs.unlinkSync('./data.txt')




/// cb __________________________________________________________________________________


// fs.writeFile('./data.txt', `Esto es un ejemplo`, 'utf-8', (err, resultado)=>{
//     if (err) return console.log('Error al escribir el archivo')   
// } )



// const fecha = new Date().toLocaleDateString()

// fs.writeFile('./fyh.txt', ``, 'utf-8', (err)=>{
//     if (err) return console.log(err)        
//     // acciones 
//     console.log('ARchivo creado') 
//     fs.writeFile('fyh.txt', 'utf-8', (err,data)=>{
//         if (err) return console.log(err)        
//         console.log(data)

//     })
    
// } )

// fs.appendFile('./data.txt', 'esto es un agregado \n', 'utf-8', err => {
//     if (err) {
//         console.log(err)        
        
//     } else {
//         console.log('Texto agregado')
        
//     }
// })

// let contenido = null
// fs.readFile('./data.txt', 'utf-8', (err, data) => {
//     if (err) {
//         console.log(err)        
        
//     } else {
//         // contenido = data
//         console.log('read', data)
        
//     }
// })


/* *|CURSOR_MARCADOR|* */
// fs.unlink('./data.txt', (err) => {
//     if (err) {
//         console.log(err)        
        
//     } else {
//         // contenido = data
//         console.log('archivo eliminado')
        
//     }
// })





/// promesas ___________________________________________________________________





const {promises : fs} = require('fs')

// fs.writeFile('./data.txt', 'esto es un ejemplo \n', 'utf-8' )
// .then(()=> console.log('Termino de escribir'))
// .catch(err => console.log(err))

// const manejoArchivo  = async () =>{
//     await fs.writeFile('./data.txt', 'esto es un ejemplo \n', 'utf-8' )    
// }
// manejoArchivo()

//______________________________________________________________________________



// const manejoArchivo  = async () =>{
//     const usuarios = [
//         {id: 1, name:'federico', email: 'f@gmail.com'}
//     ]

    // let usuariosStrngfy = JSON.stringify(usuarios)
    // console.log(usuariosStrngfy)
    // let usuarioParse = JSON.parse(usuariosStrngfy)
    // console.log(usuarioParse)
    
    // leer archivo
    // try {
    //     let data = await fs.promises.readFile('./package.json', 'utf-8' ) 
    //     let dataJs = JSON.parse(data)
    //     dataJs.author = 'Federico Osandon'
    //     console.log(dataJs)       
    //     await fs.promises.writeFile('./data.txt', JSON.stringify(dataJs,null, 2),  'utf-8' ) 
    //     console.log('Termino de escribir')
    // } catch (error) {
    //     console.log(error)
    // }

    // // escribir archivo
    // try {
    //     await fs.promises.writeFile('./data.txt', data, 'utf-8' ) 
    //     console.log('Termino de escribir')       
    // } catch (error) {
    //     console.log(error)
    // }
    
    
    

    // // agregar al  archivo
    // try {
    //     await fs.promises.appendFile('./data.txt', 'Esto es un agregado \n','utf-8' ) 
    //     // leer archivo        
    //     console.log('data agregada')       
    // } catch (error) {
    //     console.log(error)
    // }


    

    // // eliminar archivo
    
    // try {
    //     await fs.promises.unlink('./data.txt' ) 
    //     console.log('Archivo borrado')       
    // } catch (error) {
    //     console.log(error)
    // }
// }

// json
// {
//     'nombre': 'nombre'
// }



// manejoArchivo()

// class Calse {
//     constructor(){
//         this.path = './data.txt'
//     }
// }

// new Clase()
