//__________________________________________________________________________
const { Server } = require('socket.io')

const app = express()
const PORT = 8080

const httpServer = app.listen(PORT,()=>{
    console.log(`Escuchando en el puerto: ${PORT}`)
})

const io = new Server(httpServer)


// hbs __________________________________________________________________
const handlebars = require('express-handlebars')

app.engine('handlebars', handlebars.engine())
app.set('views', __dirname+'/views')
app.set('view engine', 'handlebars')
// hbs __________________________________________


app.use(express.json()) 
app.use(express.urlencoded({extended: true}))
// console.log(__dirname+'/public')
app.use('/static', express.static(__dirname+'/public'))
// mid de tercero
app.use(cookieParser())



app.get('/chat', (req, res)=>{
    res.render('chat', {})
})

io.on('connection', socket => {
    console.log('Nuevo cliente conectado')
    console.log(socket.id)
    // socket.on('message', data => {
    //     console.log(data)
    // })

    // socket.emit('evento-para-socket-individual', 'Este mensaje lo va a recibir el socket del cliente')

    // socket.broadcast.emit('evt-p-todos-menos-el-socket-actual', 'Evento que veran todos los socket menos el actual')

    // io.emit('evt-para-todos', 'Este mensaje lo reciben todos los socket conectados')

    let logs = []
    socket.on("message1",data=>{
        io.emit('log',data)
    })

    //Message2 se utiliza para la parte de almacenar y devolver los logs completos.
    socket.on("message2",data=>{
        
        logs.push({socketid:socket.id,message:data})

        io.emit('log',{logs});
    })
})

app.use((err, req, res, next)=>{
    console.log(err)
    res.status(500).send('Todo mal')
})




