const express = require('express')
const cookieParser = require('cookie-parser')
// import express from 'express'
const userRouter = require('./routes/users.router')
const productRouter = require('./routes/products.router')
const { uploader } = require('./utils/multer')


const app = express()

// hbs __________________________________________
const handlebars = require('express-handlebars')

app.engine('handlebars', handlebars.engine())
app.set('views', __dirname+'/views')
app.set('view engine', 'handlebars')
// hbs __________________________________________


app.use(express.json()) 
app.use(express.urlencoded({extended: true}))
// console.log(__dirname+'/public')
app.use('/static', express.static(__dirname+'/public'))
// mid de tercero
app.use(cookieParser())

app.use( (req, res, next)=>{
    console.log('mid app - time: ', Date.now())
    next()
} )

function mid1(req, res, next) {
    // req.dato1='dato uno'

    res.send('No tenes permiso para ver los usuarios')
}

function mid2(req, res, next) {
    req.dato2='dato dos '
    next()
}

const users = [
    {
      nombre: 'Juan',
      apellido: 'Pérez',
      edad: 25,
      correo: 'juanperez@gmail.com',
      telefono: '555-1234'
    },
    {
      nombre: 'María',
      apellido: 'García',
      edad: 30,
      correo: 'mariagarcia@gmail.com',
      telefono: '555-5678'
    },
    {
      nombre: 'Pedro',
      apellido: 'López',
      edad: 20,
      correo: 'pedrolopez@gmail.com',
      telefono: '555-9012'
    },
    {
      nombre: 'Ana',
      apellido: 'Ramírez',
      edad: 35,
      correo: 'anaramirez@gmail.com',
      telefono: '555-3456'
    },
    {
      nombre: 'Luis',
      apellido: 'Fernández',
      edad: 40,
      correo: 'luisfernandez@gmail.com',
      telefono: '555-7890'
    }
  ];

app.get('/vista', (req, res)=>{

    let user = users[Math.floor( Math.random() * users.length )]

    let testUser = {
        title: 'Mercadito Fede',
        user
    }

    res.render('index', testUser)
})


// http://localhost:8080 /api/usuarios
app.use('/api/usuarios',  userRouter)

app.use('/api/productos', productRouter)
// app.use('/api/carrito', carritoRouter)

app.post('/single', uploader.single('myfile'), (req, res)=>{
    res.status(200).send({
        status: 'success',
        message: 'se subió correctamente'
    })
})

app.use((err, req, res, next)=>{
    console.log(err)
    res.status(500).send('Todo mal')
})


const PORT = 8080

app.listen(PORT,()=>{
    console.log(`Escuchando en el puerto: ${PORT}`)
})

