const { Router } = require('express')
const { 
    getCarts, 
    getCart, 
    addProductToCart, 
    createCart, 
    deleteProductFromCart, 
    deleteCart, 
    createTicket 
} = require('../controllers/carts.controller')

const router = Router()

router
    .get('/', getCarts)
    .get('/:cid', getCart)
    .post('/', createCart)
    .put('/:cid/products/:pid', addProductToCart)
    .delete('/:cid/products/:pid', deleteProductFromCart)
    .delete('/:cid', deleteCart)
    .post('/:cid/purchase', createTicket)


module.exports = router