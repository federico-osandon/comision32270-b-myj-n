const { UserDao, ProductDao, OrderDao } = require('../Dao/factory.js') // Daos - Manager
const { ProductModel } = require('../Dao/mongo/models/product.model.js') //SchemaModel 

const ProductRepositories = require('./product.respositories.js') // Service
const UserRpositories = require('./user.respositories.js')
const OrderRepositories = require('./ordersService.js')


const userService = new UserRpositories(new UserDao())
const productService = new ProductRepositories(new ProductDao(ProductModel))
const orderService = new OrderRepositories(new OrderDao())

module.exports = {
    userService,
    productService,
    orderService
}