const MongoStore = require('connect-mongo')
const { connect } = require('mongoose')
const dotenv = require('dotenv')
const { commander } = require('../utils/commander')

// const environment = 'development'

const { mode } = commander.opts()

dotenv.config({
    path: mode === 'development' ? './.env.development' : './.env.production'
})

// const url = 'mongodb+srv://federicoaosandon:Federico1@cluster0.an130di.mongodb.net/coderhouse?retryWrites=true&w=majority'
const url = process.env.MONGO_URL 
// console.log('env',url)

let configObject = {
    port: process.env.PORT || 8080, 
    mongoUrl: process.env.MONGO_URL, 
    adminName:process.env.ADMIN_NAME,
    adminPassword:process.env.ADMIN_PASSWORD,
    dbConnection:  async () => {
        try {
            await connect(url)
            console.log('DB conectada')  
        } catch (error) {
            console.log(error)
            process.exit()
        }        
    },
    session: {
        store: MongoStore.create({
            mongoUrl: url,
            mongoOptions: {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            },
            ttl: 15000000000
        }), 
        secret: 's3cr3t0',
        resave: false,
        saveUninitialized: false,
    }
}

// console.log(configObject)

module.exports = { configObject }