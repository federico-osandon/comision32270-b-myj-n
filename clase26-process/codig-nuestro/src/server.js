// const express = require('express')
const express = require('express')
const cookieParser = require('cookie-parser')
const handlebars = require('express-handlebars')
const logger = require('morgan')
// session_______________________________________________________________
const session = require('express-session')
// socket io _______________________________________________________________
const { configObject } = require('./config/config.js')
// socket io _______________________________________________________________
require('dotenv').config()

const { Server: HttpServer } = require('http')
const { Server: ServerIo } = require('socket.io')
const { initProductsSocket } = require('./utils/productsSocketIo.js')
const { router } = require('./routes')
// passport
const { initializePassport } = require('./middleware/passport.middleware.js')
const passport = require('passport')


const app = express()
const httpServer = new HttpServer(app)
const io = new ServerIo(httpServer)

// oncección con la base de datos mongo __________________________________________________________________
configObject.dbConnection()

app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(logger('dev'))
app.use(cookieParser())
// passport
initializePassport()
app.use(passport.initialize())

// session mongo_______________________________________________________________
app.use(session(configObject.session))

app.use('/virtual' ,express.static(__dirname+'/public'))

// handlebars_______________________________________________________________
app.engine('handlebars', handlebars.engine())
app.set('views', __dirname+'/views')
app.set('view engine', 'handlebars')
 

app.use(router)

app.use((err, req, res, next)=>{
    console.log(err)
    res.status(500).send('Todo mal')
})

// socket_______________________________________________________________
initProductsSocket(io)

module.exports = {
    httpServer
}


