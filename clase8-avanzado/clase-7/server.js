// const express = require('express')
const express = require('express')

const app = express()
const PORT = 8080

app.use(express.json())
app.use(express.urlencoded({extended:true}))

const arrayUsuarios = [ // db
    { id: '1', nombre: 'nombre 1', apellido: 'apellido 1', genero: 'F' },
    { id: '2', nombre: 'nombre 2', apellido: 'apellido 2', genero: 'F' },
    { id: '3', nombre: 'nombre 3', apellido: 'apellido 3', genero: 'M' },
    { id: '4', nombre: 'nombre 4', apellido: 'apellido 4', genero: 'F' },
    { id: '5', nombre: 'nombre 5', apellido: 'apellido 5', genero: 'M' },
    { id: '6', nombre: 'nombre 6', apellido: 'apellido 6', genero: 'M' },
    { id: '7', nombre: 'nombre 7', apellido: 'apellido 7', genero: 'F' },
    { id: '8', nombre: 'nombre 8', apellido: 'apellido 8', genero: 'M' }
]

// http://localhost:8080/api/usuarios
app.get('/api/usuarios', (request, response) =>{
    response.status(200).send(arrayUsuarios)
})

// http://localhost:8080/api/usuarios
app.post('/api/usuarios', (request, response) =>{
    //mada el  cliente request 
    let user = request.body
    if (!user.nombre || !user.apellido) {
        return response.status(400).send({ message: 'Che pasar todos los datos'})
    }
    // console.log('user post',user)
    arrayUsuarios.push(user)
    console.log(arrayUsuarios)
    response.status(201).send({ 
        user,
        message: 'usuario creado' 
    })
})
// http://localhost:8080/api/usuarios/:id
app.put('/api/usuarios/:userId', (request, response) =>{

    const { userId } = request.params
    // venga el id
    const index = arrayUsuarios.findIndex(user => user.id === userId)
    // exista el usuario 
    if (index === -1) {
        return response.status(400).send({ message: 'No se encuentra el usuario'})
    }

    //mada el  cliente request 
    let user = request.body
    if (!user.nombre || !user.apellido) {
        return response.status(400).send({ message: 'Che pasar todos los datos'})
    }

    // console.log('user post',user)
    arrayUsuarios[index] = user
    console.log(arrayUsuarios)

    response.status(201).send({ 
        users: arrayUsuarios,
        message: 'usuario Modificado' 
    })
})


app.delete('/api/usuarios/:userId', (req, res)=> {
    const { userId } = req.params

    let arrayTamanno = arrayUsuarios.length
    console.log(arrayTamanno)
    let users = arrayUsuarios.filter(user => user.id !== userId )
    console.log(users.length)
    if (users.length === arrayTamanno) {
        res.status(404).send({ message:"Usuario no encontrado" })
    }
    res.status(200).send({ message:"Usuario borrado", users })
})






app.listen(PORT,err =>{
    if (err)  console.log(err)
    console.log(`Escuchando en el puerto ${PORT}`)
})