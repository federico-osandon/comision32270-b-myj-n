const express = require('express')
const app = express()

let pets = []

const petsRouter = express.Router()
app.use('/api/pets', petsRouter)

petsRouter.post('/', (req, res) => {
    const { name, species } = req.body;
    if (!name || !species) {
      res.status(400).send('El nombre y la especie son obligatorios');
      return;
    }
    const pet = { name, species };
    pets.push(pet);
    res.status(201).json(pet);
})

petsRouter.get('/:pet', (req, res) => {
    const name = req.params.pet;
    const regex = /^[a-zA-Z\s]+$/;
    if (!regex.test(name)) {
      res.status(400).send('El nombre de la mascota debe contener solo letras y espacios');
      return;
    }
    const pet = pets.find(p => p.name.toLowerCase() === name.toLowerCase());
    if (!pet) {
      res.status(404).send('No se encontró la mascota');
      return;
    }
    req.pet = pet;
    res.json(pet);
})

petsRouter.put('/:pet', (req, res) => {
    const pet = req.pet;
    if (!pet) {
      res.status(404).send('No se encontró la mascota');
      return;
    }
    pet.adopted = true;
    res.json(pet);
})

petsRouter.param('pet', (req, res, next, name) => {
    const pet = pets.find(p => p.name.toLowerCase() === name.toLowerCase());
    req.pet = pet;
    next();
});