import usersModel from "./models/User.js";

export default class Users {
   /* This is a class called `Users` that exports four methods: */
    getUsers = () =>{
        return usersModel.find();
    }
    getUserById = (id) =>{
        return usersModel.findOne({_id:id});
    }

    getUserByEmail = (email) =>{
        return usersModel.findOne({email})
    }

    saveUser = (user) =>{
        return usersModel.create(user);
    }
}